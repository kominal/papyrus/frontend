export interface Translation {
	_id?: string;
	tenantId: string;
	projectName?: string | null;
	type: string;
	key: string;
	values: { language: string; value: string }[];
}
