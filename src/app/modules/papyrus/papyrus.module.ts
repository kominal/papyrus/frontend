import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { FormModule } from '@kominal/lib-angular-form';
import { TableModule } from '@kominal/lib-angular-table';
import { PapyrusClientModule } from '@kominal/papyrus-angular-client';
import { MaterialModule } from 'src/app/shared/material.module';
import { TranslationComponent } from './pages/translation/translation.component';
import { TranslationsComponent } from './pages/translations/translations.component';
import { PapyrusRoutingModule } from './papyrus-routing.module';
import { PapyrusComponent } from './papyrus.component';

@NgModule({
	declarations: [PapyrusComponent, TranslationsComponent, TranslationComponent],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		FormModule,
		PapyrusClientModule,
		MaterialModule,
		PapyrusRoutingModule,
		TableModule,
		AngularEditorModule,
	],
})
export class PapyrusModule {}
